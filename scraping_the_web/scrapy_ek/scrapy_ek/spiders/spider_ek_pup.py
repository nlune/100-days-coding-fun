#!/usr/bin/env python
import scrapy
from scrapy_ek.items import ScrapyEkItem 

class postSpider(scrapy.Spider):
    name = "crawler" 
    def start_requests(self): 
        urls = [
                'https://www.ebay-kleinanzeigen.de/s-hunde/osnabrueck/c134l3117'
                ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):

        info = ScrapyEkItem()
        info['link'] = response.xpath("//a[@class='ellipsis']/@href").extract()
        info['cost'] = response.xpath("//div[@class='aditem-details']/strong[1]/text()").extract()
        info['date'] =  response.xpath('//div[@class="aditem-addon"]//text()').extract()
        info['desc'] = response.xpath('//div[@class="aditem-main"]/p[1]/text()').extract()
        breed = response.xpath('//p[@class="text-module-end"]/span/text()').extract()

        yield info
        print('link ', len(info['link']), ' cost ', len(info['cost']), ' date ', len(info['date']), ' desc ', len(info['desc']))
        # response.css(".aditem-addon").extract()

        # for item in zip(link,cost,date,description):
        #     scraped_info = {
        #             'date': item[2],
        #             'cost': item[1],
        #             'link': item[0] #,
        #            # 'description': item[3]
        #             }

        #     yield scraped_info 

