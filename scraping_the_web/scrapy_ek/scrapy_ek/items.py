# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ScrapyEkItem(scrapy.Item):
    # define the fields for your item here like:
    date  = scrapy.Field()
    cost = scrapy.Field()
    desc = scrapy.Field()
    link = scrapy.Field() 
