#!/usr/bin/env python
import scrapy

class postSpider(scrapy.Spider):
    name = "puppies" 
    def start_requests(self): 
        urls = [
                'https://www.ebay-kleinanzeigen.de/s-hunde/c134'
                ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):

        link = response.xpath("//a[@class='ellipsis']/@href").extract()
        cost = response.xpath("//div[@class='aditem-details']/strong[1]/text()").extract()
        date =  response.xpath('//div[@class="aditem-addon"]//text()').extract()
        description = response.xpath('//div[@class="aditem-main"]/p[1]/text()').extract()
        breed = response.xpath('//p[@class="text-module-end"]/span/text()').extract()

        # response.css(".aditem-addon").extract()

        for item in zip(link,cost,date,description):
            scraped_info = {
                    'date': item[2],
                    'cost': item[1],
                    'link': item[0] #,
                   # 'description': item[3]
                    }

            yield scraped_info 

