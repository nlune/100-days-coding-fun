#!/usr/bin/env python3
from arith_code import * 
import pickle
import os

got_ans = False
print("Hello,", end=" " )

while not got_ans: 

    resp = input("please press 'e' if you'd like to encode a msg. Press 'd' to decode. Press 'q' to exit. \n")

    if resp == "q":
        break 
    elif resp == "e": 
        string = input("Please enter the message you want to encode. \n")
        while len(string) < 1: 
            string = input("You should actually write a message if you're going to encode it \n")

        code, prob_dict, N = encode(string)
        filename = input("Your encoded msg will be saved. Please enter name for file \n")
        while len(filename) < 1:
            filename = input("I didn't get that... Pls enter a filename \n")

        with open(filename, 'wb') as f: 
            pickle.dump([code, prob_dict, N], f)
            print("Successfully saved!")
            break 
    elif resp == "d":
        filename = input("Please type in the name of file containing encoding. \n")
        if not os.path.isfile(filename):
            print("This file doesn't exist...")
            filename = input("One more chance - try again \n")
            if not os.path.isfile(filename):
                print("Nope, still not a file.")
                break
        with open(filename, 'rb') as f:
            code, prob_dict, N = pickle.load(f)
            print("The decoded message is: ")
            print(decode(code, prob_dict, N))
        break 
    else: 
        print("Sorry, I didn't understand...", end=" ")

