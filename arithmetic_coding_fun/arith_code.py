#!/usr/bin/env python
import numpy as np
import math
from decimal import getcontext
from decimal import Decimal

getcontext().prec = 1500

def get_probability_range_dict(letter, lower, cum_probability):
    # create dictionary with unique letter key and tuple for probability range. 
    # takes unique letters array, lower bound decimal, and cumulative probability array
    rangeprob_dict = dict([(l[0], (Decimal(cum_probability[i-1]), Decimal(l[1]))) if i>0 else (l[0], (Decimal(lower), Decimal(l[1]))) for i,l in enumerate(zip(letter, cum_probability))])

    return rangeprob_dict 

def get_new_probabilities(lower, upper, probability_table): 
    # returns the new cumulative probability array within the new probability range bounded by lower & upper decimals, using probablity_table array
    r = upper - lower
    # calc probabilities between new range, add to lower bound 
    new_prob = probability_table*r 

    return np.cumsum(new_prob) + lower

def decode_get_range(code, letters, cum_probability): 
    # given the decimal code, binary search style to find letter with range it falls under, using unique letters and cumulative prob. arrays 
    curr_idx = len(letters) // 2
    l = 0
    r = len(letters) - 1

    for i in range(len(letters)):
        # case where upper bound of current probability is lower than code
        if code > cum_probability[curr_idx]:
            # check if code less than upper bound of next idx, meaning it falls in range of next letter
            if code <= cum_probability[curr_idx + 1]: 
                return letters[curr_idx + 1] 
            # otherwise get idx between current idx and right side bound and keep searching
            else: 
                l = curr_idx 
                curr_idx = curr_idx + math.ceil((r - l) / 2)
                continue 

        # case where upper bound of curr letter is higher
        if code <= cum_probability[curr_idx]: 
            # if first letter, then falls within range 
            if curr_idx == 0: 
                return letters[curr_idx]
            # check if code is greater than upper bound of previous letter, then it falls in range of curr letter
            elif code > cum_probability[curr_idx - 1]: 

                return letters[curr_idx]
            # otherwise halve idx and keep searching
            else: 
                r = curr_idx 
                curr_idx = curr_idx - math.ceil((r - l) / 2)



def encode(string):
    # takes in string input and returns decimal code, prob_dict dictionary, and N int representing length of encoded string 
    N = len(string)
    arr = list(string)
    
    letter, count = np.unique(arr, return_counts=True)
    # get probability of ea. value 
    probability_table  = np.array([ Decimal(c/sum(count)) for c in count ])
    prob_dict = dict([(l,p) for l,p in zip(letter, probability_table)]) 
    cum_probability = np.cumsum(probability_table)
    
    rangeprob_dict = get_probability_range_dict(letter, 0, cum_probability)
    
    # loop through ea. value in string and calc code
    for l in arr: 
        lower = Decimal(rangeprob_dict[l][0])
        upper = Decimal(rangeprob_dict[l][1])
        new_cum_prob = get_new_probabilities(lower, upper, probability_table) 
        rangeprob_dict = get_probability_range_dict(letter, lower, new_cum_prob)

    code = (lower + upper) / 2 
    return code, prob_dict, N 


def decode(code, prob_dict, N): 
    # return original string using decimal code, prob_dict dictionary, N int representing length of encoded string
    charlist = []
    letters = [*prob_dict.keys()]
    probability_table = np.array([*prob_dict.values()])
    cum_probability = np.cumsum(probability_table)
    rangeprob_dict = get_probability_range_dict(letters, 0, cum_probability)

    for n in range(N): 
        char = decode_get_range(code, letters, cum_probability)
        charlist.append(char)
        # get new probabilities within range of decoded letter 
        lower = rangeprob_dict[charlist[n]][0]
        upper = rangeprob_dict[charlist[n]][1]
        cum_probability = get_new_probabilities(lower,upper,probability_table) 

        rangeprob_dict = get_probability_range_dict(letters, lower, cum_probability)

    solution = ''.join([str(e) for e in charlist])

    return solution 
        



